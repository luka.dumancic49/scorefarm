import React, { useEffect } from "react";
import { Provider, useDispatch, useSelector } from "react-redux";
import "./App.css";
import Router from "./Router";
import Navigation from "./components/Navigation";
import store, { persistor } from "./store";
import { getFutureMatches } from "./actions/matchActions";
import { matchesSelector } from "./selectors";
import { PersistGate } from 'redux-persist/integration/react'

function App() {
  const dispatch = useDispatch();
  const matches = useSelector(matchesSelector);

  useEffect(() => {
    getFutureMatches(dispatch)().then();
  }, []);

  useEffect(() => {
    console.log(matches);
  }, [matches])

  
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router>
          <Navigation />
        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;
