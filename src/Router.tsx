import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./layouts/Home";
import History from "./layouts/History";

export default function Router(props: any) {
  return (
    <BrowserRouter>
      <div>
        {props.children}
        <div className="content">
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/history" exact>
              <History />
            </Route>
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
}
