import { Event, Team } from '../types';
import axios from 'axios';
import * as constants from '../constants';

export const ADD_TEAM = "ADD_TEAM";
export const GET_TEAMS = "GET_TEAMS";

export const addTeam = (events: Event) => ({
  type: ADD_TEAM,
  paylod: events
});

export const getTeams = (dispatch: any) => {
  return async () => {
    const res = await axios.get(constants.GET_TEAMS, {});
    if (res && res.status === 200) {
      res.data.forEach((team: Team) => {
        dispatch({
          type: ADD_TEAM,
          paylod: team
        })
      })
    } 
  }
}


