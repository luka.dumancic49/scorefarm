import * as constants from '../constants';
import axios from 'axios';
import { VoteType, Match } from '../types';

export const ADD_FUTURE_MATCH = "ADD_FUTURE_MATCH";
export const MATCH_VOTE = "MATCH_VOTE";
export const REMOVE_MATCHES = "REMOVE_MATCHES";

export const getFutureMatches = (dispatch: any) => {
    return async () => {
      const res = await axios.get(constants.GET_FUTURE_MATCHES_PRED, {});
      if (res && res.status === 200) {
        dispatch({
          type: REMOVE_MATCHES,
        })
        res.data.forEach((match: Match) => {
          dispatch({
            type: ADD_FUTURE_MATCH,
            payload: match
          })
        })
      } 
    }
  }

  export const voteMatch = (dispatch: any) => {
    return async (matchId: number, voteType: VoteType) => {
      const res = await axios.patch(constants.VOTE_URL + matchId, {win: voteType});
      if (res && res.status === 200) {
        return dispatch(({
          type: MATCH_VOTE,
          payload: {
            matchId: matchId,
            voteType: voteType
          }
        }));
      } 
    }
  }
