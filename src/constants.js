export const BACKEND_BASE_URL = "https://scorefarm.herokuapp.com/"
export const VOTE_URL = BACKEND_BASE_URL + "api/match/predictions/"
export const GET_TEAMS = BACKEND_BASE_URL + "api/team"
export const GET_FUTURE_MATCHES_PRED = BACKEND_BASE_URL + "api/match/futureMatchesPred"
export const GET_LAST_MATCH = BACKEND_BASE_URL + "api/match/lastMatchup"

