import { createSelector } from "reselect";
import { Store } from "../types";

const state = (state: Store) => state;
const events = (state: Store) => state.events;
const teams = (state: Store) => state.teams;
const matches = (state: Store) => state.matches;


const stateSelector = createSelector(state, state => state);
const eventsSelector = createSelector(events, eventsState => eventsState);
const teamsSelector = createSelector(teams, teamsState => teamsState);
const matchesSelector = createSelector(matches, matchesState => matchesState);


export default stateSelector;
export { eventsSelector, teamsSelector, matchesSelector };
