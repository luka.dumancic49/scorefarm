import React from "react";
import BigEvent from "../components/BigEvent";
import Events from "../components/Events";

export default function() {
  return (
    <div>
      <BigEvent />
      <Events />
    </div>
  );
}
