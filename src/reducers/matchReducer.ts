import { Action, MatchState, VoteType } from "../types";
import { ADD_FUTURE_MATCH, MATCH_VOTE, REMOVE_MATCHES } from "../actions/matchActions";

const matchState: MatchState = {
  matches: [],
  userVotes: []
};

const matchReducer = (state = matchState, action: Action) => {
  switch (action.type) {
    case ADD_FUTURE_MATCH:
      let m = state.matches.find(match => match.id === action.payload.matchId);
      if (m) {
        m = action.payload;
      } else {
        state.matches.push(action.payload);
      }
      return {
        ...state
      };
      case REMOVE_MATCHES: 
        return {
          ...state,
          matches: [],
        }
      case MATCH_VOTE:
        const match = state.matches.find(match => match.id === action.payload.matchId);
        if(match == null) return state
        state.userVotes.push({
          matchId: action.payload.matchId,
          win: action.payload.voteType
        });
        switch(action.payload.voteType){
          case VoteType.homeWin:
            match.homeWins++
            break
          case VoteType.draw:
            match.draw++
            break
          case VoteType.awayWin:
            match.awayWins++
            break
        }
        return {
          ...state
        };
    default:
      return state;
  }
};

export default matchReducer;