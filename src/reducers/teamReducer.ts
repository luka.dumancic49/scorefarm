import { Action, TeamState } from "../types";
import { ADD_TEAM, GET_TEAMS } from "../actions/teamActions";

const eventState: TeamState = {
  teams: []
};

const teamReducer = (state = eventState, action: Action) => {
  switch (action.type) {
    case ADD_TEAM:
      state.teams.push(action.payload);
      return {
        ...state
      };
    default:
      return state;
  }
};

export default teamReducer;
