import { combineReducers } from "redux";
import eventReducer from "./eventReducer";
import teamReducer from './teamReducer';
import matchReducer from "./matchReducer";

export default combineReducers({
  events: eventReducer,
  teams: teamReducer,
  matches: matchReducer
});
