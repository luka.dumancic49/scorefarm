import { Action, EventState, VoteType } from "../types";
import { ADD_EVENTS } from "../actions/eventActions";
const eventState: EventState = {
  events: [
    {
      id: 0,
      homeTeam: {
        id: 1,
        name: "Osijek",
      },
      awayTeam: {
        id: 2,
        name: "Hajduk"
      },
      votesHome: 12,
      votesDraw: 20,
      votesAway: 30,
      date: new Date()
    }
  ]
};

const eventReducer = (state = eventState, action: Action) => {
  switch (action.type) {
    case ADD_EVENTS:
      state.events.push(action.payload);
      return {
        ...state
      };
    default:
      return state;
  }
};

export default eventReducer;
