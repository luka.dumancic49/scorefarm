export interface Store {
  events: EventState;
  teams: TeamState;
  matches: MatchState
}

export interface EventState {
  events: Event[];
}

export interface TeamState {
  teams: Team[];
}

export interface UserVote {
  matchId: any;
  win: any;
}

export interface MatchState {
  matches: Match[];
  userVotes: UserVote[];
}

export interface Team {
  id: number;
  name: string;
  elo?: number;
}


export interface Match {
  id: number;
  homeTeam: Team;
  awayTeam: Team;
  result: number;
  score: string;
  eventTime: Date;
  homeWins: number;
  draw: number;
  awayWins: number;
  homeWinPred: number;
  drawPred: number;
  awayWinPred: number;
  homeWinEloPred: number,
  drawEloPred: number,
  awayWinEloPred: number
}

export interface Event {
  id: number;
  homeTeam: Team;
  awayTeam: Team;
  votesHome: number;
  votesDraw: number;
  votesAway: number;
  date: Date;
}

export enum ClubPosition {
  Home,
  Away
}

export interface Action {
  type: string;
  payload: any;
}

export enum Clubs {
  def = "default",
  hajduk = 'hajduk',
  dinamo = 'dinamo',
  lucko = 'lucko',
  osijek = 'osijek',
  rijeka = 'rijeka',
  gorica = 'gorica',
  inter = 'inter',
  varazdin = 'varazdin',
  lokomotiva = 'lokomotiva',
  istra = 'istra',
  slaven = 'slaven',
}

export enum VoteType {
  homeWin = 0,
  awayWin = 1,
  draw = 2
}