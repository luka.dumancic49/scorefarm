import React from "react";
import { Clubs } from "../types";

const clubImages = {
  [Clubs.def]: require('../assets/clubs/default.png'),
  [Clubs.dinamo]: require('../assets/clubs/dinamo.png'),
  [Clubs.hajduk]: require('../assets/clubs/hajduk.png'),
  [Clubs.osijek]: require('../assets/clubs/osijek.png'),
  [Clubs.rijeka]: require('../assets/clubs/rijeka.png'),
  [Clubs.inter]: require('../assets/clubs/inter.png'),
  [Clubs.slaven]: require('../assets/clubs/slaven.png'),
  [Clubs.gorica]: require('../assets/clubs/gorica.png'),
  [Clubs.istra]: require('../assets/clubs/istra.png'),
  [Clubs.varazdin]: require('../assets/clubs/varazdin.png'),
  [Clubs.lokomotiva]: require('../assets/clubs/lokomotiva.png'),
  [Clubs.lucko]: require('../assets/clubs/lucko.png'),
}

interface PropTypes {
  club: Clubs;
  name: string;
  home?: boolean;
}

export default function BigClub(props: PropTypes) {
  return (
    <div className="big-club">
      <img className='big-club-img' src={clubImages[props.club]} alt="logo" />
      <div className="club-banner-container">
        <img className="club-banner" src={props.home ? require('../assets/home-banner.png') : require('../assets/away-banner.png')} alt="logo" />
        <div className="club-name-container">
          <span className="club-name">{props.name}</span>
        </div>
      </div>
    </div>
  );
}
