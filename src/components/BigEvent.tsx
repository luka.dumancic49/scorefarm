import React from "react";
import BigClub from "./BigClub";
import { Clubs, VoteType, ClubPosition, Team } from "../types";
import { useSelector } from "react-redux";
import { matchesSelector } from "../selectors";
import LinearPrediction from "./EventContainer/LinearPrediction";
import NumberOfVotes from "./EventContainer/NumberOfVotes";
export default function BigEvent() {
  const matchesState = useSelector(matchesSelector);
  const match = matchesState.matches[0];

  function getClub(team: Team): Clubs{
    switch(team.id){
      case 1:
        return Clubs.hajduk
      case 2:
        return Clubs.slaven
      case 3:
        return Clubs.dinamo
      case 9:
        return Clubs.varazdin
      case 10:
        return Clubs.inter
      case 12:
        return Clubs.lokomotiva
      case 13:
        return Clubs.istra
      case 14:
        return Clubs.osijek
      case 16:
        return Clubs.rijeka
      case 21:
        return Clubs.gorica
      default:
        return Clubs.def
    }
  }


  if (!match) {
    return null;
  }
  return (
    <>
      <img
        className="background"
        src={require("../assets/stadium.png")}
        alt="logo"
      />
      <div className="big-event">
        <div className="grad1" />
        <div className="grad2" />
        <div className="content">
          <h1>Vote for your favourite team to see predictions</h1>
          <div className="big-vote-container">
            <div className="big-vote">
              <BigClub club={getClub(match.homeTeam)} name={match.homeTeam.name} home={true} />
              <img className="x" src={require("../assets/x.png")} alt="x" />
              <BigClub club={getClub(match.awayTeam)} name={match.awayTeam.name} home={false} />
            </div>
            <div className="big-votes">
              <NumberOfVotes
                clubPosition={ClubPosition.Home}
                club={match.homeTeam}
                matchId={match.id}
                numOfVotes={match.homeWins}
                isVoted={
                  matchesState.userVotes.filter(
                    (v) => v.matchId === match.id && v.win === VoteType.homeWin
                  ).length > 0
                }
              />
              <NumberOfVotes
                clubPosition={ClubPosition.Away}
                club={match.awayTeam}
                matchId={match.id}
                numOfVotes={match.awayWins}
                isVoted={
                  matchesState.userVotes.filter(
                    (v) => v.matchId === match.id && v.win === VoteType.awayWin
                  ).length > 0
                }
              />
            </div>
            <div
              style={{
                width: 630,
                left: '22%',
                marginTop: 20,
                position: 'absolute'
              }}
            >
              <LinearPrediction
                homePrediction={match.homeWinPred}
                awayPrediction={match.awayWinPred}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
