import React from "react";
import { NavLink } from "react-router-dom";

export default function Navigation() {
  return (
    <nav>
      <img src={require('../assets/logo.png')} alt="logo"/>
      <ul>
        <li>
          <NavLink activeClassName="active" to="/" exact>Vote</NavLink>
        </li>
        <li>
          <a onClick={() => {
            (document as any).getElementById('predictions').scrollIntoView() 
          }}>Predictions</a>
        </li>
        <li>
          <NavLink activeClassName="active" to="/history">History</NavLink>
        </li>
      </ul>
    </nav>
  );
}
