import React, { useState } from "react";
import { useSelector } from "react-redux";
import { matchesSelector } from "../selectors";
import EventContainer from "../components/EventContainer";
import Switch from "react-switch";
import { Match } from "../types";

function shuffle(a: any) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

export default function (props: any) {
  const matchesState = useSelector(matchesSelector);
  const allMatches = matchesState.matches.filter((m) => {
    if (props.user) {
      if (matchesState.userVotes.filter((v) => v.matchId === m.id).length) {
        return true;
      }
      return false;
    }
    return true;
  });
  const dates = [
    ...(new Set(
      allMatches
        .sort((a, b) => new Date(a.eventTime) as any - (new Date(b.eventTime) as any))
        .map((m) => new Date(m.eventTime).toLocaleDateString("hr"))
    ) as any),
  ];
  console.log(dates);
  const [elo, setElo] = useState(true);
  return (
    <div className="events">
      <div className="events-header">
        <h2>{!props.user ? "Predictions for this week" : "My votes"}</h2>
        <div className="switch-pred-container">
          <span>STAT</span>
          <label>
            <Switch
              checkedIcon={false}
              uncheckedIcon={false}
              offColor={"#7dabf5"}
              onColor={"#65bfaf"}
              onChange={() => setElo(!elo)}
              checked={elo}
            />
          </label>
          <span>ELO</span>
        </div>
      </div>
      {dates
        .map((d) =>
          allMatches.filter(
            (m) => new Date(m.eventTime).toLocaleDateString("hr") === d
          )
        )
        .map((matches) => (
          <div>
            <h3>{new Date(matches[0].eventTime).toLocaleDateString("hr")}</h3>
            {allMatches.map((match: Match, index: number) => {
              if (index > 4) {
                return null;
              }
              return (
                <EventContainer
                  key={match.id}
                  match={match}
                  userVotes={matchesState.userVotes}
                  isElo={elo}
                />
              );
            })}
          </div>
        ))}
    </div>
  );
}
