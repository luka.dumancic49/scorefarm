import React from "react";
import { ClubPosition, Team } from "../../types";

interface EventContainerProps {
  club: Team;
  clubPosition: ClubPosition;
}

export default function(props: EventContainerProps) {
  const isHome = props.clubPosition === ClubPosition.Home;
  return (
    <div style={{ [isHome ? 'marginRight' : 'marginLeft']: 20, width: 120, textAlign: isHome ? 'left' : 'right' }}>
      <span
        style={{
          color: isHome ? "#3BA7F4" : "#ED713C"
        }}
      >
        {props.club.name}
      </span>
    </div>
  );
}
