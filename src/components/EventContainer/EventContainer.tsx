import React, { useState, useEffect } from "react";
import { ClubPosition, Match, UserVote, VoteType } from "../../types";
import TeamName from "./TeamName";
import LinearPrediction from "./LinearPrediction";
import NumberOfVotes from "./NumberOfVotes";
import Axios from "axios";
import { GET_LAST_MATCH } from "../../constants";
import { useSelector } from "react-redux";
import { matchesSelector } from "../../selectors";

interface EventContainerProps {
  match: Match;
  userVotes: UserVote[];
  isElo: boolean;
}

const getMatchData = async (id: any, matchesState: any, setData: any) => {
  const res = await Axios.get(`${GET_LAST_MATCH}/${id}`);
  setData({
    ...matchesState.matches.find((m: Match) => m.id === id),
    ...res.data,
  });
};

export default function (props: EventContainerProps) {
  const matchesState = useSelector(matchesSelector);
  const [isOpen, setIsOpen] = useState(false);
  const [data, setData] = useState<Match | null>(null);

  useEffect(() => {
    if (isOpen && !data) {
      getMatchData(props.match.id, matchesState, setData).then();
    }
  }, [isOpen]);

  return (
    <>
      <div
        id="predictions"
        className="event-container"
        style={
          isOpen
            ? {
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
              }
            : {}
        }
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        <div style={{ flexGrow: 1, display: "flex", alignItems: "center" }}>
          <TeamName
            clubPosition={ClubPosition.Home}
            club={props.match.homeTeam}
          />
          <NumberOfVotes
            clubPosition={ClubPosition.Home}
            club={props.match.homeTeam}
            matchId={props.match.id}
            numOfVotes={props.match.homeWins}
            isVoted={
              props.userVotes.filter(
                (v) =>
                  v.matchId === props.match.id && v.win === VoteType.homeWin
              ).length > 0
            }
          />
        </div>
        <LinearPrediction
          homePrediction={props.isElo ? props.match.homeWinEloPred : props.match.homeWinPred}
          awayPrediction={props.isElo ? props.match.awayWinEloPred : props.match.awayWinPred}
        />
        <div
          style={{
            flexGrow: 1,
            display: "flex",
            justifyContent: "end",
            alignItems: "center",
          }}
        >
          <NumberOfVotes
            clubPosition={ClubPosition.Away}
            club={props.match.awayTeam}
            matchId={props.match.id}
            numOfVotes={props.match.awayWins}
            isVoted={
              props.userVotes.filter(
                (v) =>
                  v.matchId === props.match.id && v.win === VoteType.awayWin
              ).length > 0
            }
          />
          <TeamName
            clubPosition={ClubPosition.Away}
            club={props.match.awayTeam}
          />
        </div>
      </div>
      {isOpen && data ? (
        <div className="old-prediction">
          <div
            style={{
              flexBasis: 246,
            }}
          >
            <h3>LAST PREDICTION</h3>
            <p>
              {new Date(data ? data.eventTime : 0).toLocaleDateString("hr")}
            </p>
          </div>
          <div
            style={{
              paddingLeft: 92,
              paddingRight: 92,
              textAlign: "center",
            }}
          >
            <h3>{data.score}</h3>
            {data.score && (
              <p>
                Last prediction was in favour of{" "}
                {data.homeWinPred == data.awayWinPred
                  ? "noone"
                  : data.homeWinPred > data.awayWinPred
                  ? data.homeTeam.name
                  : data.awayTeam.name}{" "}
                with {Math.round((data.homeWinPred - data.awayWinPred) * 100)}%
                more chance of winning the game. Final result was {data.score}{" "}
                for{" "}
                {data.score.split(" : ").map((v) => parseInt(v, 10))[0] ==
                data.score.split(" : ").map((v) => parseInt(v, 10))[1]
                  ? "noone"
                  : data.score.split(" : ").map((v) => parseInt(v, 10))[0] >
                    data.score.split(" : ").map((v) => parseInt(v, 10))[1]
                  ? data.homeTeam.name
                  : data.awayTeam.name}
              </p>
            )}
          </div>
          <div
            style={{
              flexBasis: 246,
            }}
          >
            {data.score && (
              <img
                src={
                  [
                    require("../../assets/check-circle-solid 2.png"),
                    require("../../assets/check-circle-solid 2(1).png"),
                  ][
                    data.homeWinPred == data.awayWinPred
                      ? data.score
                          .split(" : ")
                          .map((v) => parseInt(v, 10))[0] ==
                        data.score.split(" : ").map((v) => parseInt(v, 10))[1]
                        ? 0
                        : 1
                      : data.homeWinPred > data.awayWinPred
                      ? data.score.split(" : ").map((v) => parseInt(v, 10))[0] >
                        data.score.split(" : ").map((v) => parseInt(v, 10))[1]
                        ? 0
                        : 1
                      : data.score.split(" : ").map((v) => parseInt(v, 10))[0] <
                        data.score.split(" : ").map((v) => parseInt(v, 10))[1]
                      ? 0
                      : 1
                  ]
                }
              />
            )}
          </div>
        </div>
      ) : null}
    </>
  );
}
