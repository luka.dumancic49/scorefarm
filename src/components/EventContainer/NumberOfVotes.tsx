import React, { useState } from "react";
import { ClubPosition, Team, VoteType } from "../../types";
import { faUserFriends } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch } from "react-redux";
import { matchesSelector } from '../../selectors'
import { voteMatch } from '../../actions/matchActions'

interface EventContainerProps {
    club: Team;
    clubPosition: ClubPosition;
    matchId: number;
    numOfVotes: number;
    isVoted: boolean;
}

export default function(props: EventContainerProps) {
    const dispatch = useDispatch();
    const voteType = props.clubPosition === ClubPosition.Home ? VoteType.homeWin : VoteType.awayWin;
    const [isVoting, setIsVoting] = useState(false);

    const isHome = props.clubPosition === ClubPosition.Home;
    const isVoted = props.isVoted;

    const buttonStyle = {
        width: "85px",
        height: "40px",
        background: isVoted ? '#FFF' : isHome ? "#3BA7F4" : "#ED713C",
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)",
        borderRadius: "12px",
        color: !isVoted ? '#FFF' : isHome ? "#3BA7F4" : "#ED713C",
        fontStyle: "normal",
        fontWeight: 700,
        fontSize: "14px",
        lineHeight: "14px",
        cursor: "pointer",
        border: isVoted ? `1px solid ${isHome ? "#3BA7F4" : "#ED713C"}` : "none"
    }

    const icon =
        <FontAwesomeIcon
            icon={faUserFriends}
            style={{
                color: !isVoted ? '#FFF' : isHome ? "#3BA7F4" : "#ED713C",
                width: 15,
                height: 12,
                marginRight: 8
            }}
        />

    const vote = (event: any) => {
        event.stopPropagation();
        if (!isVoted && !isVoting) {
            setIsVoting(true);
            voteMatch(dispatch)(props.matchId, voteType);
        }
    }

    return (
        <div>
            <button style={buttonStyle} onClick={vote}>
                {icon}
                {props.numOfVotes}
            </button>
        </div>
    );
}
