import React from "react";
import { ClubPosition, Team } from "../../types";

interface EventContainerProps {
    clubPosition: ClubPosition;
    numberOfVotes: number;
}

export default function(props: EventContainerProps) {
    const isHome = props.clubPosition === ClubPosition.Home;
    const buttonStyle = {
        width: "40px",
        height: "40px",
        background: isHome ? "#3BA7F4" : "#ED713C",
        boxShadow: "0px 8px 16px rgba(0, 0, 0, 0.05)",
        borderRadius: "12px",
        color: "white",
        fontStyle: "normal",
        fontWeight: 900,
        fontSize: "14px",
        lineHeight: "40px",
        border: 'none'
    }
    return (
        <div>
            <button className = "numbersOfButton" style={buttonStyle}>
                {props.numberOfVotes}
            </button>
        </div>
    );
}
