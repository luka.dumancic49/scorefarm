import React from 'react';
import OurEstimate from './OurEtimate';
import { ClubPosition } from '../../types';

interface LinearPredictionProps {
  homePrediction: number;
  awayPrediction: number;
}

export default (props: LinearPredictionProps) => {
  const { homePrediction, awayPrediction } = props;

  return <div style={{ flexGrow: 2.5, display: 'flex', justifyContent: 'center', alignItems: 'center', position: 'relative' }}>
    <OurEstimate clubPosition={ClubPosition.Home} numberOfVotes={Math.round(homePrediction*100)}/>
    <div style={{ marginLeft: 20, marginRight: 20, borderRadius: 8, width: 355, height: 12, flexDirection: 'row', display: 'flex'}}>
      <div style={{ background: '#3BA7F4', height: 12, flexGrow: Math.round(homePrediction*100), borderRadius: '8px 0px 0px 8px' }}></div>
      <div style={{ background: '#e0dfdc', height: 12, flexGrow: Math.round((1 - homePrediction - awayPrediction)*100)}}></div>
      <div style={{ background: '#ED713C', height: 12, flexGrow: Math.round(awayPrediction*100), borderRadius: '0px 8px 8px 0px'}}></div>
    </div>
    <OurEstimate clubPosition={ClubPosition.Away} numberOfVotes={Math.round(awayPrediction*100)} />
    <img src={require("../../assets/v1.svg")} style={{ position: 'absolute', left: 130 + 355 * homePrediction, top: 0}}/>
    <img src={require("../../assets/v2.svg")} style={{ position: 'absolute', right: 130 + 355 * awayPrediction, top: 0}}/>
  </div>

}