## Requirements

* Installed node enviroment 
Install node here [https://nodejs.org/en/download/](https://nodejs.org/en/download/)

* Installed yarn package manager 
Install yarn here [https://classic.yarnpkg.com/en/docs/install](https://classic.yarnpkg.com/en/docs/install)


## Available Scripts

In the project directory, you can run:

### `yarn`

Install all modules needed<br />
You will have to run this first time


### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


## Learn More

To learn React, check out the [React documentation](https://reactjs.org/).
